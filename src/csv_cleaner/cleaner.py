"""
This is a skeleton file that can serve as a starting point for a Python
console script. To run this script uncomment the following lines in the
``[options.entry_points]`` section in ``setup.cfg``::

    console_scripts =
         fibonacci = csv_cleaner.skeleton:run

Then run ``pip install .`` (or ``pip install -e .`` for editable mode)
which will install the command ``fibonacci`` inside your current environment.

Besides console scripts, the header (i.e. until ``_logger``...) of this file can
also be used as template for Python modules.

Note:
    This file can be renamed depending on your needs or safely removed if not needed.

References:
    - https://setuptools.pypa.io/en/latest/userguide/entry_point.html
    - https://pip.pypa.io/en/stable/reference/pip_install
"""

import logging
import sys
import csv
import difflib
from enum import Enum
from unidecode import unidecode

from csv_cleaner import __version__

__author__ = "Benjamin Forest"
__copyright__ = "Benjamin Forest"
__license__ = "MIT"

_logger = logging.getLogger(__name__)


class CsvMergerStrategy(Enum):
    INTERACTIVE = 1
    ALWAYS_LEFT = 2


class CsvMerger:

    def __init__(self, comparison_keys, merge_strategy=CsvMergerStrategy.ALWAYS_LEFT):
        self.comparison_keys = comparison_keys
        self.merged = {}
        self.merge_strategy = merge_strategy

    def compute_key(self, row):
        return unidecode("".join([row[k] for k in self.comparison_keys]).replace(" ", "").lower())
    
    def compute_key_sorted(self, row):
        return "".join(sorted(self.compute_key(row)))

    def always_use_left(self, k, row, other, merged):
        merged[k] = row[k]
        return merged
    
    def interactive_merge(self, k, row, other, merged):
        """ask user how to merge two rows

        Args:
            k (_type_): key we cannot auto merge
            v (_type_): value corresponding
            row (_type_): row we are merging
            other (_type_): row we are merging
            merged (_type_): result of merge

        Returns:
            _type_: _description_
        """
        diff = difflib.ndiff(row[k], other[k])
        diffstr = ''.join(diff)
        print(f"Cannot merge rows, diff is : {diffstr}")
        print(f"Both {row[k]} and {other[k]} have values for key {k} (row key is {self.compute_key(row)})!")
        answer = input(f"What do we do ?\n   1: use {row[k]}\n   2: use {other[k]},\n   3: enter new value,\n   4: drop row,\n   5: merge values, 6: change column (new key ?)")
        if answer == 1:
            merged[k] = row[k]
        elif answer == 2:
            merged[k] = other[k]
        elif answer == 3:
            merged[k] = input("Type your value: ")
        elif answer == 4:
            return None
        elif answer == 5:
            merged[k] = row[k] + " or " + other[k]
        elif answer == 6:
            newKey = input("Key to use")
            merged[k] = row[k]
            if merged[newKey]:
                print(f"Overriding {merged[newKey]}")
            merged[newKey] = other[k]
        else:
            print("Invalid answer")
        return merged

    def merge_rows(self, row, other):
        merged = {}
        for k, v in row.items():
            if not v:
                merged[k] = other[k]
            elif other[k]:
                if v == other[k]:
                    merged[k] = v
                else:
                    if self.merge_strategy == CsvMergerStrategy.ALWAYS_LEFT:
                        merged = self.always_use_left(k, row, other, merged)
                    elif self.merge_strategy == CsvMergerStrategy.INTERACTIVE:
                        merged = self.interactive_merge(k, row, other, merged)
            else:
                merged[k] = v
        return merged
    
    def append_row(self, row):
        key = self.compute_key_sorted(row)
        print(f"computed key : {key}")
        if key in self.merged:
            merged_row =  self.merge_rows(self.merged[key], row)
            if merged_row:
                self.merged[key] = merged_row
            else:
                del self.merged[key]
        else:
            self.merged[key] = row

# ---- Python API ----
# The functions defined in this section can be imported by users in their
# Python scripts/interactive interpreter, e.g. via
# `from csv_cleaner.skeleton import fib`,
# when using this Python module as a library.


def clean_csv(filename, merge_strategy=CsvMergerStrategy.ALWAYS_LEFT):
    merger = CsvMerger(['First Name', 'Middle Name', 'Last Name'], merge_strategy)
    with open(filename, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            merger.append_row(row)


if __name__ == "__main__":
    # ^  This is a guard statement that will prevent the following code from
    #    being executed in the case someone imports this file instead of
    #    executing it as a script.
    #    https://docs.python.org/3/library/__main__.html

    # After installing your project with pip, users can also run your Python
    # modules as scripts via the ``-m`` flag, as defined in PEP 338::
    #
    #     python -m csv_cleaner.skeleton 42
    #
    clean_csv(sys.argv[1], CsvMergerStrategy.INTERACTIVE)
