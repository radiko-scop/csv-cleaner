import requests
import csv

def concat(string1, string2):
    return string1+string2

def parse_csv(text):
    """Convert a string containing csv data into a list of parsed rows.

    Args:
        text (_type_): _description_

    Returns:
        _type_: _description_
    """
    return list(csv.reader(text.splitlines()))

def get_csv_sample(url):
    res = requests.get(url)
    return parse_csv(res.text)


def get_csv_sample_10000():
    """return 10000 organisation from https://github.com/datablist/sample-csv-files?tab=readme-ov-file

    Returns:
        _type_: a list of parsed rows
    """
    return get_csv_sample("https://drive.google.com/uc?id=13p-box0F9kou4wE9AyeBNKMSfE767xT-&export=download")


def get_csv_sample_100():
    """return 100 organisation from https://github.com/datablist/sample-csv-files?tab=readme-ov-file

    Returns:
        _type_: a list of parsed rows
    """
    return get_csv_sample('https://drive.google.com/uc?id=13a2WyLoGxQKXbN_AIjrOogIlQKNe9uPm&export=download')